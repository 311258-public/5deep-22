# 5DEEP

Projet contenant les fichiers pertinents au projet 5DEEP.

Le notebook est pleinement compatible (et son exécution est recommendée) avec Google Colab. *Conseil: [utilisez VSCode localement en tandem avec une instance Colab](https://colab.research.google.com/github/JayThibs/jacques-blog/blob/master/_notebooks/2021-09-27-connect-to-colab-from-local-vscode.ipynb)!*
